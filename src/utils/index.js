import {strToU8,zlibSync,strFromU8,unzlibSync} from 'fflate'

/**
 * 防抖
 * @param {*} fn 
 * @param {*} n 
 * @returns 
 */
export function debounce(fn,n=100){
  let handle;
  return (...args)=>{
    if(handle)clearTimeout(handle);
    handle = setTimeout(()=>{
      fn(...args)
    },n)
  }
}

/**
 * 将字符串转换为base64
 * @param {*} data 
 * @returns 
 */
export function utoa(data){
  //将字符串转成Uint8Array
  const buffer = strToU8(data);
  //以最大的压缩级别进行压缩，返回的zipped也是一个Uint8Array
  const zipped = zlibSync(buffer,{level:9});
  //将Uint8Array重新转换成二进制字符串
  const binary = strFromU8(zipped,true);
  //将二进制字符串编码为Base64编码字符串（内置的方法不支持Unicode编码）
  return btoa(binary);
}

/**
 * 将base64转换为字符串
 * @param {*} base64 
 * @returns 
 */
export function atou(base64){
  // 将base64转成二进制字符串
  const binary = atob(base64)
   // 检查是否是zlib压缩的数据，zlib header (x78), level 9 (xDA)
  if (binary.startsWith('\x78\xDA')) {
    // 将字符串转成Uint8Array
    const buffer = strToU8(binary, true);
     // 解压缩
    const unzipped = unzlibSync(buffer);
    //将Uint8Array重新转换成字符串
    return strFromU8(unzipped);
  }
  //兼容没有使用压缩的数据
  return decodeURIComponent(escape(binary));
}