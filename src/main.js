import { createApp } from 'vue'
import App from './App.vue'

//引入bytemd样式
import 'bytemd/dist/index.css'

createApp(App).mount('#app')
