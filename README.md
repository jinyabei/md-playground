# md-playground 实现md在线编辑

## 参考文档
- Power by  [bytemd](https://github.com/bytedance/bytemd), inspired by [Vue SFC Playground](https://sfc.vuejs.org/)

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```
